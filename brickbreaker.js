/**
 *  The brick breaker application
 *
 *  @namespace BrickBreaker
 */
BrickBreaker = new function()
{
  //########################################
  //Private member variables
  //########################################

  //DOM Nodes
  var DOM = {
    platform: null,
    ball: null,
    bricks: null,
    message: null
  };

  //Messages
  var messages = {
    start: 'Press the spacebar to start the game',
    round_win: 'Round won! Press the spacebar to go to the next level',
    won: 'You\'ve won! Press the spacebar to play again',
    lost: 'Uh Oh you lost! Press the spacebar to play again'
  };

  /**
   *  Each level contains 4 rows of at most 8 bricks
   *
   *  Brick Values:
   *    0: none
   *    1: default
   *    2: strong block (2 hits needed)
   *    3: invuln block
   */
  var levels = [

    //Starting level
    [
      [0, 0, 1, 1, 1, 1, 0, 0],
      [0, 1, 1, 2, 2, 1, 1, 0],
      [0, 1, 1, 1, 1, 1, 1, 0],
      [0, 0, 1, 1, 1, 1, 0, 0]
    ],

    //Mid Level
    [
      [0, 1, 2, 2, 2, 2, 1, 0],
      [1, 2, 2, 3, 3, 2, 2, 1],
      [2, 1, 2, 2, 2, 2, 1, 2],
      [0, 3, 1, 2, 2, 1, 3, 0]
    ],

    //Avd Level
    [
      [2, 2, 1, 1, 1, 1, 2, 2],
      [2, 2, 1, 2, 2, 1, 2, 2],
      [2, 2, 2, 3, 3, 2, 2, 2],
      [3, 1, 3, 3, 3, 3, 1, 3]
    ]
  ];

  //Current level index
  var current_level = 0;

  //Is the game in progress?
  var in_progress = false;

  /**
   *  When building the bricks based on the level, use this array to store brick info
   *
   *  Each item in the array should match the following format:
   *
   *  {
   *    elem: The HTMLElement
   *    rect: The rect provided by getBoundingClientRect(),
   *    type: The brick type:
   *      1: default
   *      2: strong
   *      3: invuln block
   *  }
   */
  var bricks = [];

  /**
   *  Keep track of the ball and platform directions
   *
   *  Value:
   *    -1: Ball or platform moving left
   *    0: Ball or platform not moving
   *    1: Ball or platform moving right
   */
  var tracking = {
    ball_x: 1,
    ball_y: 1,
    plat_x: 0
  };

  /**
   *  Modify the speed of the ball or platform
   *  A frame is rendered at roughly 60fps
   *
   *  1 second of movement = modifier * 60
   */
  var modifiers = {
    ball_x: 5,
    ball_y: 7,
    plat_x: 20
  };

  //########################################
  //Private member functions
  //########################################

  /**
   *  Initialize our first level
   *
   *  @private
   *
   *  @return {undefined}
   */
  function initialize()
  {
    //Get DOM nodes
    //TODO

    //Start
    //TODO

    //Animate
    //TODO
  }

  /**
   *  Start the brick breaker game
   *
   *  @return {undefined}
   */
  function start(arg_message)
  {
    //Position platform
    positionPlatform();

    //Position ball
    positionBall();

    //Build the bricks
    buildBricks();
  }

  /**
   *  Reset the game
   *
   *  @private
   *
   *  @return {undefined}
   */
  function reset()
  {
    in_progress = false;
    tracking = {
      ball_x: 1,
      ball_y: 1,
      plat_x: 0
    };

    return start();
  }

  /**
   *  Show a message
   *
   *  @private
   *
   *  @param  {string} arg_message The message key to display, null hides the message
   *
   *  @return {undefined}
   */
  function showMessage(arg_message)
  {
    var message = messages[arg_message];
    if (!message) return DOM.message.classList.remove('active');

    DOM.message.innerHTML = message;
    DOM.message.classList.add('active');
  }

  /**
   *  Build the brick DOM nodes for the current level
   *
   *  @private
   *
   *  @return {undefined}
   */
  function buildBricks()
  {
    /**
     *  TODO:
     *
     *  When adding elements to the <div id="bricks"> container follow this format:
     *    No Brick: <div class="empty"></div>
     *    Normal Brick: <div></div>
     *    Strong Brick: <div class="strong"></div>
     *    Strong HIT Brick: <div class="strong hit"></div>
     *    Invuln Brick: <div class="invuln"></div>
     *
     *  Be sure to:
     *    - Add brick elements to container
     *    - Set classes as needed
     *    - Store brick information in the bricks[] array
     */
  }

  //########################################
  //Collision Detection
  //########################################

  /**
   *  Are two rects intersecting?
   *
   *  @private
   *
   *  @param  {object} arg_r1 The first rect
   *  @param  {object} arg_r2 The second rect
   *
   *  @return {boolean} True if they intersect, false otherwise
   */
  function isIntersecting(arg_r1, arg_r2)
  {
    //TODO
  }

  /**
   *  Should the ball reverse the X direction when it hits/intersects with another object
   *
   *  @private
   *
   *  @param  {object} arg_brect The ball rect
   *  @param  {object} arg_orect The object rect
   *
   *  @return {boolean} True if the ball should reverse direction, false otherwise
   */
  function shouldReverseX(arg_brect, arg_orect)
  {
    var reverse = false;

    //Hit right or left side?
    //TODO

    //If we hit the object center mass DONT reverse the direction of the ball
    //TODO

    return reverse;
  }

  /**
   *  Should the ball reverse the Y direction when it hits/intersects with another object
   *
   *  @private
   *
   *  @param  {object} arg_brect The ball rect
   *  @param  {object} arg_orect The object rect
   *
   *  @return {boolean} True if the ball should reverse direction, false otherwise
   */
  function shouldReverseY(arg_brect, arg_orect)
  {
    var reverse = false;

    //Hit top or bottom side?
    //TODO

    //If we hit the object center mass DONT reverse the direction of the ball
    //TODO

    return reverse;
  }

  /**
   *  Checks the ball for any collisions between bricks, the platform, or the screen
   *
   *  The platform should not go past the screen boundaries
   *  If the ball hits an object it should change its direction
   *  If the ball goes past the platform and goes past the bottom edge, the game is over
   *  If the ball hits a brick:
   *    If its a normal brick, change the ball direction and hide the brick
   *    If its a strong brick, after one additional hit, change the ball direction and hide the brick
   *    If its an invuln brick, change the ball direction
   *
   *  @return {undefined}
   */
  function checkCollisions()
  {
    if (!in_progress) return;

    //Check screen collisions
    checkScreenCollisions();

    //Check platform collisions
    checkPlatformCollisions();

    //Check brick collisions
    checkBrickCollisions();
  }

  /**
   *  Check ball and platform collisions with the screen
   *
   *  @private
   *
   *  @return {undefined}
   */
  function checkScreenCollisions()
  {
    /**
     *  TODO
     *
     *  This function checks collisions between the ball/platform and the viewport
     *
     *  Since the platform only moves horizontally, you only need to check the X direction and use positionPlatform
     *  The ball moves in both the X and Y directions so be sure to set the X/Y tracking accordingly and use positionBall
     *
     *  If the ball goes PAST the platform and hits the bottom edge of the screen the game is over, reset the level to 0 and show a 'lost' message
     */
  }

  /**
   *  Check the ball for collisions with the platform
   *
   *  @private
   *
   *  @return {undefined}
   */
  function checkPlatformCollisions()
  {
    /**
     *  TODO
     *
     *  This function checks collisions between the ball and the platform
     *
     *  Be sure to use isIntersecting and shouldReverseX/shouldReverseY to change the ball tracking modifier
     *  Also be sure to account for the platform movement that can also change the ball direction
     */
  }

  /**
   *  Checks the ball for collisions with any visibile bricks and handle hit tracking
   *
   *  @private
   *
   *  @return {undefined}
   */
  function checkBrickCollisions()
  {
    /**
     *  TODO
     *
     *  This function checks collisions between the ball and the bricks[] array
     *
     *  Be sure to use isIntersecting and shouldReverseX/shouldReverseY to change the ball tracking modifier
     *  When a brick is hit be sure to set/remove/add the correct classes
     *  Follow this format:
     *    Normal -> Empty
     *    Strong HIT -> Normal
     *    Strong -> Strong HIT
     *    Invuln -> Invuln
     */
  }

  //########################################
  //Event Handlers
  //########################################

  /**
   *  A keydown event was caught, handle it
   *
   *  @private
   *
   *  @param  {Event} arg_event The event object
   *
   *  @return {undefined}
   */
  function onKeydown(arg_event)
  {
    /**
     *  TODO
     *
     *  Handle a keydown event, follow this format:
     *
     *  Space: Start the game and clear any visible messages
     *  Escape: Reset the game
     *  Left/Right: Move the platform by changing the tracking modifier
     */
  }

  /**
   *  A keyup event was caught, handle it
   *
   *  @private
   *
   *  @param  {Event} arg_event The event object
   *
   *  @return {undefined}
   */
  function onKeyup(arg_event)
  {
    /**
     *  TODO
     *
     *  Handle a keyup event, follow this format:
     *
     *  Left/Right: Stop moving the platform
     */
  }

  /**
   *  This function is called each time a frame should be rendered
   *
   *  @private
   *
   *  @return {undefined}
   */
  function doAnimate()
  {
    /**
     *  TODO
     *
     *  This function handles all animation
     *
     *  Be sure to:
     *    - If the platform is moving, position it
     *    - Position the ball based on the tracking and modifiers
     *    - Check for collisions using checkCollisions()
     *    - Check to see if there are no more bricks and either go to the next round or show a win message and start over
     */
  }

  //########################################
  //Element Positioning
  //########################################

  /**
   *  Position the platform
   *
   *  @private
   *
   *  @param  {number} arg_position The positive or negative number to position the platform
   *                                If the value is positive, it moves the platform that many pixels to the right
   *                                If the value is negative, it moves the platform that many pixels to the left
   *                                If the value is not a number, it centers the platform
   *  @param  {boolean} arg_increment Defaults to true, increment the positions rather than setting
   *
   *  @return {undefined}
   */
  function positionPlatform(arg_position, arg_increment)
  {
    //TODO
  }

  /**
   *  Position the ball
   *
   *  @private
   *
   *  @param  {number} arg_left The positive or negative number to position the ball horizontally
   *                            If the value is positive, it moves the ball that many pixels to the right horizontally
   *                            If the value is negative, it moves the ball that many pixels to the left horizontally
   *                            If the value is not a number, it centers the ball above the platform
   *  @param  {number} arg_top The positive or negative number to position the ball vertically
   *                           If the value is positive, it moves the ball that many pixels to the right vertically
   *                           If the value is negative, it moves the ball that many pixels to the left vertically
   *                           If the value is not a number, it centers the ball 20px above the platform
   *  @param  {boolean} arg_increment Defaults to true, increment the positions rather than setting
   *
   *  @return {undefined}
   */
  function positionBall(arg_left, arg_top, arg_increment)
  {
    //TODO
  }

  //########################################
  //Initialization
  //########################################

  //Listeners
  window.addEventListener('DOMContentLoaded', initialize, false);
  window.addEventListener('keydown', onKeydown, false);
  window.addEventListener('keyup', onKeyup, false);
};
