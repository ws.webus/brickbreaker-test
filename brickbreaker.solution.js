/**
 *  The brick breaker application
 *
 *  @namespace BrickBreaker
 */
BrickBreaker = new (function() {
  //########################################
  //Private member variables
  //########################################

  //DOM Nodes
  var DOM = {
    platform: null,
    ball: null,
    bricks: null,
    message: null
  };

  //Messages
  var messages = {
    start: "Press the spacebar to start the game",
    round_win: "Round won! Press the spacebar to go to the next level",
    won: "You've won! Press the spacebar to play again",
    lost: "Uh Oh you lost! Press the spacebar to play again",
    pause: "Press Esc to continue"
  };

  // Key Codes
  var KEY_CODES = {
    left: 37,
    right: 39,
    esc: 27,
    space: 32
  };

  var platform_bounds = {
    x: 0,
    y: 0,
    width: 150,
    height: 20
  };

  var ball_bounds = {
    x: 0,
    y: 0,
    width: 12,
    height: 12
  };

  /**
   *  Each level contains 4 rows of at most 8 bricks
   *
   *  Brick Values:
   *    0: none
   *    1: default
   *    2: strong block (2 hits needed)
   *    3: invuln block
   */
  var levels = [
    //Starting level
    [
      [0, 0, 1, 1, 1, 1, 0, 0],
      [0, 1, 1, 2, 2, 1, 1, 0],
      [0, 1, 1, 1, 1, 1, 1, 0],
      [0, 0, 1, 1, 1, 1, 0, 0]
    ],

    //Mid Level
    [
      [0, 1, 2, 2, 2, 2, 1, 0],
      [1, 2, 2, 3, 3, 2, 2, 1],
      [2, 1, 2, 2, 2, 2, 1, 2],
      [0, 3, 1, 2, 2, 1, 3, 0]
    ],

    //Avd Level
    [
      [2, 2, 1, 1, 1, 1, 2, 2],
      [2, 2, 1, 2, 2, 1, 2, 2],
      [2, 2, 2, 3, 3, 2, 2, 2],
      [3, 1, 3, 3, 3, 3, 1, 3]
    ]
  ];

  //Current level index
  var current_level = 0;

  //Is the game in progress?
  var in_progress = false;

  /**
   *  When building the bricks based on the level, use this array to store brick info
   *
   *  Each item in the array should match the following format:
   *
   *  {
   *    elem: The HTMLElement
   *    rect: The rect provided by getBoundingClientRect(),
   *    type: The brick type:
   *      1: default
   *      2: strong
   *      3: invuln block
   *  }
   */
  var bricks = [];
  var vulnerabile_bricks = [];

  /**
   *  Keep track of the ball and platform directions
   *
   *  Value:
   *    -1: Ball or platform moving left
   *    0: Ball or platform not moving
   *    1: Ball or platform moving right
   */
  var tracking = {
    ball_x: 1,
    ball_y: -1,
    plat_x: 0
  };

  /**
   *  Modify the speed of the ball or platform
   *  A frame is rendered at roughly 60fps
   *
   *  1 second of movement = modifier * 60
   */
  var modifiers = {
    ball_x: 5,
    ball_y: 7,
    plat_x: 20
  };

  //########################################
  //Private member functions
  //########################################
  /**
   *  Initialize our first level
   *
   *  @private
   *
   *  @return {undefined}
   */
  function initialize() {
    //Get DOM nodes
    DOM.platform = document.querySelector("#platform");
    DOM.ball = document.querySelector("#ball");
    DOM.bricks = document.querySelector("#bricks");
    DOM.message = document.querySelector("#message");

    //Start
    start();

    //Animate
    doAnimate();
  }

  /**
   *  Start the brick breaker game
   *
   *  @return {undefined}
   */
  function start(arg_message) {
    in_progress = true;
    //Position platform
    positionPlatform();

    //Position ball
    positionBall();

    //Build the bricks
    buildBricks();
  }

  /**
   *  Reset the game
   *
   *  @private
   *
   *  @return {undefined}
   */
  function reset() {
    in_progress = false;
    tracking = {
      ball_x: 1,
      ball_y: -1,
      plat_x: 0
    };

    // Clear bricks field
    DOM.bricks.innerHTML = "";
    bricks = [];
    vulnerabile_bricks = [];

    return start();
  }

  /**
   *  Show a message
   *
   *  @private
   *
   *  @param  {string} arg_message The message key to display, null hides the message
   *
   *  @return {undefined}
   */
  function showMessage(arg_message) {
    var message = messages[arg_message];
    if (!message) return DOM.message.classList.remove("active");

    DOM.message.innerHTML = message;
    DOM.message.classList.add("active");
  }

  /**
   *  Build the brick DOM nodes for the current level
   *
   *  @private
   *
   *  @return {undefined}
   */
  function buildBricks() {
    /**
     *  TODO:
     *
     *  When adding elements to the <div id="bricks"> container follow this format:
     *    No Brick: <div class="empty"></div>
     *    Normal Brick: <div></div>
     *    Strong Brick: <div class="strong"></div>
     *    Strong HIT Brick: <div class="strong hit"></div>
     *    Invuln Brick: <div class="invuln"></div>
     *
     *  Be sure to:
     *    - Add brick elements to container
     *    - Set classes as needed
     *    - Store brick information in the bricks[] array
     */
    var class_types = {
      0: "empty",
      1: null,
      2: "strong",
      3: "invuln"
    };
    var fragment = document.createDocumentFragment();
    levels[current_level].forEach(arr => {
      arr.forEach(type => {
        var brick_elm = document.createElement("div");

        if (class_types[type]) {
          brick_elm.classList.add(class_types[type]);
        }

        DOM.bricks.appendChild(brick_elm);

        var brick = {
          elem: brick_elm,
          rect: brick_elm.getBoundingClientRect(),
          type: type
        };

        if (type !== 0) {
          bricks.push(brick);
        }
      });
    });

    vulnerabile_bricks = bricks.filter(brick => {
      return brick.type != 3;
    });
  }

  //########################################
  //Collision Detection
  //########################################

  /**
   *  Are two rects intersecting?
   *
   *  @private
   *
   *  @param  {object} arg_r1 The first rect
   *  @param  {object} arg_r2 The second rect
   *
   *  @return {boolean} True if they intersect, false otherwise
   */
  function isIntersecting(arg_r1, arg_r2) {
    //TODO
    var intersecting = false;
    var inter_x =
      arg_r1.x + arg_r1.width > arg_r2.x && arg_r1.x < arg_r2.x + arg_r2.width;
    var inter_y =
      arg_r1.y + arg_r1.height > arg_r2.y &&
      arg_r1.y < arg_r2.y + arg_r2.height;
    if (inter_x && inter_y) {
      intersecting = true;
    }

    return intersecting;
  }

  /**
   *  Should the ball reverse the X direction when it hits/intersects with another object
   *
   *  @private
   *
   *  @param  {object} arg_brect The ball rect
   *  @param  {object} arg_orect The object rect
   *
   *  @return {boolean} True if the ball should reverse direction, false otherwise
   */
  function shouldReverseX(arg_brect, arg_orect) {
    var reverse = false;

    //Hit right or left side?
    //TODO
    var lefthand =
      arg_brect.x + arg_brect.width - modifiers.ball_x <= arg_orect.x;
    var righthand =
      arg_brect.x + modifiers.ball_x >= arg_orect.x + arg_orect.width;
    if (lefthand || righthand) {
      reverse = true;
    }

    // Platform specific logic
    // If platform moves oposite ball direction, then reverse the ball
    if (
      (tracking.plat_x === -1 && tracking.ball_x === 1) ||
      (tracking.plat_x === 1 && tracking.ball_x === -1)
    ) {
      reverse = true;
    }

    //If we hit the object center mass DONT reverse the direction of the ball
    //TODO

    return reverse;
  }

  /**
   *  Should the ball reverse the Y direction when it hits/intersects with another object
   *
   *  @private
   *
   *  @param  {object} arg_brect The ball rect
   *  @param  {object} arg_orect The object rect
   *
   *  @return {boolean} True if the ball should reverse direction, false otherwise
   */
  function shouldReverseY(arg_brect, arg_orect) {
    var reverse = false;

    //Hit top or bottom side?
    //TODO
    var brect_higher =
      arg_brect.y + arg_brect.height >= arg_orect.y + modifiers.ball_y;
    var brect_lower =
      arg_brect.y <= arg_orect.y + arg_orect.height - modifiers.ball_y;
    var brect_on_right_side =
      arg_brect.x >= arg_orect.x + arg_orect.width - modifiers.ball_x;
    var brect_on_left_side =
      arg_brect.x + arg_brect.width <= arg_orect.x + modifiers.ball_x;
    var brect_on_any_side = brect_on_right_side || brect_on_left_side;

    if (brect_lower || brect_higher) {
      if (!brect_on_any_side) {
        reverse = true;
      }
    }

    //If we hit the object center mass DONT reverse the direction of the ball
    //TODO

    return reverse;
  }

  /**
   *  Checks the ball for any collisions between bricks, the platform, or the screen
   *
   *  The platform should not go past the screen boundaries
   *  If the ball hits an object it should change its direction
   *  If the ball goes past the platform and goes past the bottom edge, the game is over
   *  If the ball hits a brick:
   *    If its a normal brick, change the ball direction and hide the brick
   *    If its a strong brick, after one additional hit, change the ball direction and hide the brick
   *    If its an invuln brick, change the ball direction
   *
   *  @return {undefined}
   */
  function checkCollisions() {
    if (!in_progress) return;

    //Check screen collisions
    checkScreenCollisions();

    //Check platform collisions
    checkPlatformCollisions();

    //Check brick collisions
    checkBrickCollisions();
  }

  /**
   *  Check ball and platform collisions with the screen
   *
   *  @private
   *
   *  @return {undefined}
   */
  function checkScreenCollisions() {
    /**
     *  TODO
     *
     *  This function checks collisions between the ball/platform and the viewport
     *
     *  Since the platform only moves horizontally, you only need to check the X direction and use positionPlatform
     *  The ball moves in both the X and Y directions so be sure to set the X/Y tracking accordingly and use positionBall
     *
     *  If the ball goes PAST the platform and hits the bottom edge of the screen the game is over, reset the level to 0 and show a 'lost' message
     */

    //  Platform collision detection
    var platform_col_left = platform_bounds.x <= 0;
    var platform_col_right =
      platform_bounds.x + platform_bounds.width >= window.innerWidth;

    if (platform_col_left && tracking.plat_x === -1) {
      positionPlatform(0, false);
    }

    if (platform_col_right && tracking.plat_x === 1) {
      positionPlatform(window.innerWidth - platform_bounds.width, false);
    }

    // Ball collision detection
    var ball_col_right = ball_bounds.x + ball_bounds.width >= window.innerWidth;
    var ball_col_left = ball_bounds.x <= 0;
    var ball_col_top = ball_bounds.y <= 0;
    var ball_col_bottom =
      ball_bounds.y + ball_bounds.height >= window.innerHeight;

    if (ball_col_right || ball_col_left) {
      tracking.ball_x = -tracking.ball_x;
    }

    if (ball_col_top) {
      tracking.ball_y = -tracking.ball_y;
    }

    if (ball_col_bottom) {
      in_progress = false;
      showMessage("lost");
    }
  }

  /**
   *  Check the ball for collisions with the platform
   *
   *  @private
   *
   *  @return {undefined}
   */
  function checkPlatformCollisions() {
    /**
     *  TODO
     *
     *  This function checks collisions between the ball and the platform
     *
     *  Be sure to use isIntersecting and shouldReverseX/shouldReverseY to change the ball tracking modifier
     *  Also be sure to account for the platform movement that can also change the ball direction
     */
    if (!isIntersecting(ball_bounds, platform_bounds)) {
      return;
    }

    if (shouldReverseY(ball_bounds, platform_bounds)) {
      tracking.ball_y *= -1;
    }

    if (shouldReverseX(ball_bounds, platform_bounds)) {
      tracking.ball_x *= -1;
    }
  }

  /**
   *  Checks the ball for collisions with any visibile bricks and handle hit tracking
   *
   *  @private
   *
   *  @return {undefined}
   */
  function checkBrickCollisions() {
    /**
     *  TODO
     *
     *  This function checks collisions between the ball and the bricks[] array
     *
     *  Be sure to use isIntersecting and shouldReverseX/shouldReverseY to change the ball tracking modifier
     *  When a brick is hit be sure to set/remove/add the correct classes
     *  Follow this format:
     *    Normal -> Empty
     *    Strong HIT -> Normal
     *    Strong -> Strong HIT
     *    Invuln -> Invuln
     */

    bricks.forEach(brick => {
      if (brick.type === 0 || !isIntersecting(ball_bounds, brick.rect)) {
        return;
      }

      var reverseX = shouldReverseX(ball_bounds, brick.rect);
      var reverseY = shouldReverseY(ball_bounds, brick.rect);

      if (reverseX) {
        tracking.ball_x *= -1;
      }

      if (reverseY) {
        tracking.ball_y *= -1;
      }

      if (brick.type === 1) {
        brick.type = 0;
        brick.elem.classList.add("empty");

        bricks.splice(bricks.indexOf(brick), 1);
        vulnerabile_bricks.splice(bricks.indexOf(brick), 1);
      }

      if (brick.type === 4) {
        brick.type = 1;
        brick.elem.classList.remove("hit", "strong");
      }

      if (brick.type === 2) {
        brick.type = 4;
        brick.elem.classList.add("hit");
      }
    });
  }

  //########################################
  //Event Handlers
  //########################################

  /**
   *  A keydown event was caught, handle it
   *
   *  @private
   *
   *  @param  {Event} arg_event The event object
   *
   *  @return {undefined}
   */
  function onKeydown(arg_event) {
    /**
     *  TODO
     *
     *  Handle a keydown event, follow this format:
     *
     *  Space: Start the game and clear any visible messages
     *  Escape: Reset the game
     *  Left/Right: Move the platform by changing the tracking modifier
     */
    if (arg_event.keyCode === KEY_CODES.left) {
      tracking.plat_x = -1;
    } else if (arg_event.keyCode === KEY_CODES.right) {
      tracking.plat_x = 1;
    }

    if (arg_event.keyCode === KEY_CODES.esc) {
      if (in_progress) {
        showMessage("pause");
      } else {
        showMessage();
      }

      in_progress = !in_progress;
    }

    if (arg_event.keyCode === KEY_CODES.space) {
      showMessage(null);
      reset();
    }
  }

  /**
   *  A keyup event was caught, handle it
   *
   *  @private
   *
   *  @param  {Event} arg_event The event object
   *
   *  @return {undefined}
   */
  function onKeyup(arg_event) {
    /**
     *  TODO
     *
     *  Handle a keyup event, follow this format:
     *
     *  Left/Right: Stop moving the platform
     */
    if (
      arg_event.keyCode === KEY_CODES.left ||
      arg_event.keyCode === KEY_CODES.right
    ) {
      tracking.plat_x = 0;
    }
  }

  /**
   *  This function is called each time a frame should be rendered
   *
   *  @private
   *
   *  @return {undefined}
   */
  function doAnimate() {
    /**
     *  TODO
     *
     *  This function handles all animation
     *
     *  Be sure to:
     *    - If the platform is moving, position it
     *    - Position the ball based on the tracking and modifiers
     *    - Check for collisions using checkCollisions()
     *    - Check to see if there are no more bricks and either go to the next round or show a win message and start over
     */

    if (in_progress) {
      checkCollisions();

      if (tracking.plat_x != 0) {
        DOM.platform.style.left = `${platform_bounds.x}px`;
      }

      // Ball animation
      ball_bounds.x += modifiers.ball_x * tracking.ball_x;
      ball_bounds.y += modifiers.ball_x * tracking.ball_y;
      DOM.ball.style.left = `${ball_bounds.x}px`;
      DOM.ball.style.top = `${ball_bounds.y}px`;

      // No more bricks
      if (vulnerabile_bricks.length === 0 && current_level < levels.length) {
        in_progress = false;

        console.log(current_level, levels.length);
        if (current_level < levels.length - 1) {
          showMessage("round_win");
          current_level++;
        } else {
          showMessage("won");
          current_level = 0;
        }
      }

      // Position platform
      positionPlatform(modifiers.plat_x * tracking.plat_x);
    }

    requestAnimationFrame(doAnimate);
  }

  //########################################
  //Element Positioning
  //########################################

  /**
   *  Position the platform
   *
   *  @private
   *
   *  @param  {number} arg_position The positive or negative number to position the platform
   *                                If the value is positive, it moves the platform that many pixels to the right
   *                                If the value is negative, it moves the platform that many pixels to the left
   *                                If the value is not a number, it centers the platform
   *  @param  {boolean} arg_increment Defaults to true, increment the positions rather than setting
   *
   *  @return {undefined}
   */
  function positionPlatform(arg_position, arg_increment = true) {
    //TODO
    if (arg_position === undefined) {
      var screen_center = window.innerWidth / 2;
      var platfor_center = platform_bounds.width / 2;
      var platform_center_pos = screen_center - platfor_center;
      DOM.platform.style.left = `${platform_center_pos}px`;
      platform_bounds.x = platform_center_pos;
      platform_bounds.y = DOM.platform.getBoundingClientRect().top;
      return;
    }

    if (arg_increment) {
      platform_bounds.x += arg_position;
    } else {
      platform_bounds.x = arg_position;
    }
  }

  /**
   *  Position the ball
   *
   *  @private
   *
   *  @param  {number} arg_left The positive or negative number to position the ball horizontally
   *                            If the value is positive, it moves the ball that many pixels to the right horizontally
   *                            If the value is negative, it moves the ball that many pixels to the left horizontally
   *                            If the value is not a number, it centers the ball above the platform
   *  @param  {number} arg_top The positive or negative number to position the ball vertically
   *                           If the value is positive, it moves the ball that many pixels to the right vertically
   *                           If the value is negative, it moves the ball that many pixels to the left vertically
   *                           If the value is not a number, it centers the ball 20px above the platform
   *  @param  {boolean} arg_increment Defaults to true, increment the positions rather than setting
   *
   *  @return {undefined}
   */
  function positionBall(arg_left, arg_top, arg_increment = true) {
    //TODO
    if (typeof arg_left !== "number") {
      var screen_center = window.innerWidth / 2;
      var ball_center = ball_bounds.width / 2;
      var ball_center_pos = screen_center - ball_center;
      DOM.ball.style.left = `${ball_center_pos}px`;
      ball_bounds.x = ball_center_pos;
    }

    if (typeof arg_top !== "number") {
      var platform_y = DOM.platform.getBoundingClientRect().top;
      var ball_height = ball_bounds.height;
      var ball_top_pos = platform_y - ball_height - 20;
      DOM.ball.style.top = `${ball_top_pos}px`;
      ball_bounds.y = ball_top_pos;
    }
  }

  //########################################
  //Initialization
  //########################################

  //Listeners
  window.addEventListener("DOMContentLoaded", initialize, false);
  window.addEventListener("keydown", onKeydown, false);
  window.addEventListener("keyup", onKeyup, false);
})();
